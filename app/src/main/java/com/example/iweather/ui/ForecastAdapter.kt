package com.example.iweather.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.iweather.data.model.Details
import com.example.iweather.databinding.ViewForecastListRowBinding
import java.util.*

class ForecastAdapter : RecyclerView.Adapter<ForecastAdapter.ItemViewHolder>() {

    private var itemList: MutableList<Details> = mutableListOf()

    fun setItems(newList: MutableList<Details>) {
        val diffCallback = ItemDiffCallback(this.itemList, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.itemList = LinkedList(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    private fun getItem(position: Int): Details {
        return itemList[position]
    }

    inner class ItemDiffCallback(
        private val mOldList: List<Details>,
        private val mNewList: List<Details>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return mOldList.size
        }

        override fun getNewListSize(): Int {
            return mNewList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return mOldList[oldItemPosition] == mNewList[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = mOldList[oldItemPosition]
            val newItem = mNewList[newItemPosition]

            return oldItem.time == newItem.time
        }

    }

    inner class ItemViewHolder(val binding: ViewForecastListRowBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewForecastListRowBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.weather = item
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}