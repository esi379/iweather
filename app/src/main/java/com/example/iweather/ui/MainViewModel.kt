package com.example.iweather.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.iweather.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

     /** Get forecast details */
    val forecastLiveData = ::fetchForecastDetails
    private fun fetchForecastDetails(location: String) = mainRepository.getForecast(location)
        .asLiveData(viewModelScope.coroutineContext)
}