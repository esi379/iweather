package com.example.iweather.ui

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.iweather.R
import com.example.iweather.databinding.ActivityMainBinding
import com.example.iweather.utils.isNetworkConnected
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), LocationListener {

    private val mainViewModel: MainViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding

    private lateinit var locationManager: LocationManager

    private var findLocation: Boolean = false

    private val listAdapter: ForecastAdapter by lazy {
        ForecastAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI()
        initLocationTracker()
    }

    private fun initUI() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.rvForecast.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = listAdapter
        }
    }

    private fun initLocationTracker() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 100
            )
            return
        }
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            0,
            0F,
            this
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 100) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initLocationTracker()
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        if (!findLocation) {
            getForecastData(location)
            findLocation = true
        }
    }

    private fun getForecastData(location: Location) {
        mainViewModel.forecastLiveData
            .invoke("${location.latitude},${location.longitude}")
            .observe(this, {
                when {
                    it.isLoading -> loading.visibility = View.VISIBLE
                    it.isSuccess -> {
                        loading.visibility = View.GONE
                        binding.forecast = it.data
                        it.data?.daily?.data?.let { it1 -> listAdapter.setItems(it1) }
                    }
                    it.isError -> {
                        loading.visibility = View.GONE
                        Log.d("TAG", "Error: ${it.message}")

                        if (!isNetworkConnected(this)) {
                            Toast.makeText(this, "Connect to Internet", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            })
    }

    /** Convert Fahrenheit to Celsius and vice versa */
    fun convert(view: View) {
        if (binding.txtDegree.text == "℉") {
            binding.txtDegree.text = "℃"
            binding.txtTemp.text =
                ((binding.txtTemp.text.toString().toDouble() - 32.00) * (5 / 9)).toInt().toString()
        } else if (binding.txtDegree.text == "℃") {
            binding.txtDegree.text = "℉"
            binding.txtTemp.text = binding.forecast?.currently?.temperatureHigh?.toInt().toString()
        }
    }
}