package com.example.iweather.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by vv0z on 14.01.2022
 *
 * @Description : Main application class
 */
@HiltAndroidApp
class MyApplication:Application() {

}