package com.example.iweather.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.text.format.DateFormat
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import java.lang.Long.parseLong
import java.text.SimpleDateFormat
import java.util.*

fun isNetworkConnected(context: Context): Boolean {
    val cm =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    if (cm != null) {
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
    return false
}

@BindingAdapter("getDate")
fun AppCompatTextView.getDate(dateTime: Long) {
    val calendar = Calendar.getInstance(Locale.ENGLISH)
    calendar.timeInMillis = dateTime * 1000L
    text = DateFormat.format("MMM d", calendar)
}