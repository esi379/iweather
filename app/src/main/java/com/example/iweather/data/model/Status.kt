package com.example.iweather.data.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}