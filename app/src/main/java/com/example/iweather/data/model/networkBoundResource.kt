@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.example.iweather.data.model

import kotlinx.coroutines.flow.*
import retrofit2.Response

inline fun <REMOTE> networkBoundResource(
    crossinline fetch: suspend () -> Response<REMOTE>,
    crossinline shouldSaveLocal: () -> Boolean = { false },
    crossinline saveFetchResult: (REMOTE) -> Unit = {},
) = flow<Resource<REMOTE>> {

    emit(Resource.loading(null))

    try {
        flowOf(fetch()).collect {
            if (it.isSuccessful) {
                if (shouldSaveLocal()) {
                    it.body()?.let { it1 -> saveFetchResult(it1) }
                }
                emitAll(flowOf(Resource.success(it.body()!!)))
            } else {
                emitAll(flowOf(Resource.error(it.message(), null)))
            }
        }
    } catch (throwable: Throwable) {
        emit(Resource.error(msg = throwable.message!!, data = null))
    }
}