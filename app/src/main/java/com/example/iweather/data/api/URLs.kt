package com.example.iweather.data.api

object URLs {

    const val BASE_URL = "https://api.darksky.net"
    const val FORECAST_URL = "/forecast/2bb07c3bece89caf533ac9a5d23d8417/"
}