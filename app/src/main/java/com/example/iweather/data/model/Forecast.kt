package com.example.iweather.data.model

import com.google.gson.annotations.SerializedName

data class Forecast(
    @SerializedName("timezone")
    var timezone: String? = "",
    @SerializedName("currently")
    var currently: Details,
    @SerializedName("hourly")
    var hourly: Weather,
    @SerializedName("daily")
    var daily: Weather
)