package com.example.iweather.data.repository

import com.example.iweather.data.api.MainApiService
import com.example.iweather.data.model.networkBoundResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MainRepository @Inject constructor(private val mainApiService: MainApiService) {

    /** Get forecast details */
    fun getForecast(location: String) = networkBoundResource(
        fetch = {
            mainApiService.fetchForecast(location)
        }
    ).flowOn(Dispatchers.IO)
}