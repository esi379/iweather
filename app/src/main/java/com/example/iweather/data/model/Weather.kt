package com.example.iweather.data.model

import com.google.gson.annotations.SerializedName

data class Weather(
    @SerializedName("summary")
    var summary: String? = "",
    @SerializedName("data")
    var data: ArrayList<Details> = arrayListOf(),
)