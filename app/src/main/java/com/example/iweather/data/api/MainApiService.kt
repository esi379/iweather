package com.example.iweather.data.api

import com.example.iweather.data.model.Forecast
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface MainApiService {

    @GET(URLs.FORECAST_URL + "{location}")
    suspend fun fetchForecast(
        @Path("location") location: String
    ): Response<Forecast>
}