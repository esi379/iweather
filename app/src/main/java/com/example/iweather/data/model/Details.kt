package com.example.iweather.data.model

import com.google.gson.annotations.SerializedName

data class Details(
    @SerializedName("time")
    var time: Long? = 0,
    @SerializedName("summary")
    var summary: String? = "",
    @SerializedName("temperature", alternate = ["temperatureHigh"])
    var temperatureHigh: Double? = 0.0,
    @SerializedName("temperatureLow")
    var temperatureLow: Double? = 0.0,
    @SerializedName("humidity")
    var humidity: Double? = 0.0,
    @SerializedName("pressure")
    var pressure: Double? = 0.0,
    @SerializedName("windSpeed")
    var windSpeed: Double? = 0.0,
    @SerializedName("cloudCover")
    var cloudCover: Double? = 0.0,
)